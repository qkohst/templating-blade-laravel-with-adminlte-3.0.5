<?php

namespace App\Http\Controllers;

use App\Pertanyaan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', compact('data_pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pertanyaan = new Pertanyaan();
        $pertanyaan->judul   = $request->input('title');
        $pertanyaan->isi     = $request->input('content');
        $pertanyaan->save();
        return redirect('/pertanyaan/index')->with("success", "New Question Posted Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_pertanyaan = \App\Pertanyaan::find($id);
        return view('pertanyaan/edit', ['data_pertanyaan' => $data_pertanyaan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pertanyaan = \App\Pertanyaan::find($id);
        $pertanyaan->judul   = $request->input('judul');
        $pertanyaan->isi     = $request->input('isi');
        $pertanyaan->update($request->all());
        $pertanyaan->save();
        return redirect('/pertanyaan/index')->with('success', 'Question Edited Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_pertanyaan = \App\Pertanyaan::find($id);
        $data_pertanyaan->delete();
        return redirect('/pertanyaan/index')->with('success', 'Question Deleted Successfully');
    }
}
