@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Question</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="/pertanyaan/index">Question</a></li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Edit Question</h3>
      </div>
      <!-- /.card-header -->

      <div class="card-body">
        @if(session('success'))
        <div class="callout callout-success alert alert-success alert-dismissible fade show" role="alert">
          <h5><i class="fas fa-check"></i> Success :</h5>
          {{session('success')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif
        <form action="/pertanyaan/{{$data_pertanyaan->id}}/update" method="POST">
          {{csrf_field()}}
          <div class="row">
            <div class="col-4">
              <label for="judul">Title</label>
              <input name="judul" type="text" class="form-control bg-light" id="judul" value="{{$data_pertanyaan->judul}}" required>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <label for="isi">Content</label>
              <textarea name="isi" class="form-control bg-light" id="isi" rows="5" required>{{$data_pertanyaan->isi}}</textarea>
            </div>
          </div>
          <hr>
          <button type="submit" class="btn btn-success btn-sm "><i class="fas fa-save"></i> SIMPAN</button>
          <a class="btn btn-danger btn-sm" href="/pertanyaan/index" role="button"><i class="fas fa-undo"></i>
            BATAL</a>
        </form>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- Modal Tambah -->
    <div class="modal fade" id="createPertanyaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><i class="nav-icon fas fa-question-circle my-1 btn-sm-1"></i> Post A Question</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="/pertanyaan/store" method="POST">
              {{csrf_field()}}
              <div class="row">
                <label for="title">Title</label>
                <input value="{{old('title')}}" name="title" type="text" class="form-control bg-light" id="title" placeholder="Title question" required>
                <label for="semester">Contents</label>
                <textarea name="content" class="form-control bg-light" id="content" rows="5" placeholder="Contents question" required>{{old('content')}}</textarea>
              </div>
              <hr>
              <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-save"></i>
                POST</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->
@endsection