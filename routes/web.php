<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('content/index');
});
Route::get('/data-tables', function () {
    return view('content/data-tables');
});

Route::get('/pertanyaan/index', 'PertanyaanController@index');
Route::post('/pertanyaan/store', 'PertanyaanController@store');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::post('/pertanyaan/{id}/update', 'PertanyaanController@update');
Route::get('/pertanyaan/{id}/destroy', 'PertanyaanController@destroy');
